import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  x:string = 'string interpolation';

// *ngFordirective

  heroes:string[] = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado'];

  isValid:boolean = true; //*ngIf demo
  
// Two way data binding
  name: string = '';

//Event
  setValue() {
     this.name = ''; 
    }


  constructor() { }

  ngOnInit() {

    
  }

}
